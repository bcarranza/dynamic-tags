import yaml
from jinja2 import Environment, FileSystemLoader

def generate_gitlab_ci_config(tags_file, template_file, output_file):
    # Load tags from tags.txt
    with open(tags_file, 'r') as tags_file:
        tags = tags_file.read().splitlines()

    # Load Jinja template
    template_loader = FileSystemLoader(searchpath="./")
    template_env = Environment(loader=template_loader)
    template = template_env.get_template(template_file)

    # Render template with tags data
    output_text = template.render(tags=tags)

    # Write the final GitLab CI configuration to an output file
    with open(output_file, 'w') as output:
        output.write(output_text)

if __name__ == "__main__":
    generate_gitlab_ci_config("tags.txt", "template.jinja", "run-on-runners.yml")

